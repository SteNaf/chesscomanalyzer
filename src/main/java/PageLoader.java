import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PageLoader implements Runnable{
    private final WebDriver driver;
    private final String url;

    public PageLoader(WebDriver driver, String url) {
        this.driver = driver;
        this.url = url;
    }

    @Override
    public void run() {
        if (!url.startsWith("https://www.chess.com/analysis/game/")) return;

        driver.get(url);

        //If the driver is logged out stop the program as the next games will also not be analyzed
        String login = driver.findElement(By.tagName("html")).getAttribute("class");
        if (login.equals("user-logged-out")) System.exit(0);

        try {
            //If there is no player country flag, the game is invalid
            driver.findElement(By.cssSelector("div.country-flags-component"));
        }catch (NoSuchElementException e) {
            return;
        }

        //Wait until the analysis is available, after 30 seconds throw an error
        new WebDriverWait(driver, Duration.ofSeconds(30)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.tallies-component")));
    }
}
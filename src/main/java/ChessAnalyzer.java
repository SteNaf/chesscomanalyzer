import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChessAnalyzer {
    private final WebDriver driver;

    public ChessAnalyzer() {
        //Get the driver and make sure it exists.
        URL driverURL = this.getClass().getClassLoader().getResource("chromedriver.exe");
        assert driverURL != null;

        //Set the path to the driver
        System.setProperty("webdriver.chrome.driver", driverURL.getPath());
        driver = new ChromeDriver();

        //When program closes, close the driver
        Runtime.getRuntime().addShutdownHook(new Thread(driver::quit));
    }

    public void analyze() {
        //This service makes it available to input mulitple urls
        //When it will continue to analyze all games when you are done with the inputs
        ExecutorService service = Executors.newSingleThreadExecutor();

        //Login to chess.com
        //If this has any errors close the program
        try {
            new ChessLogin(driver);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        Scanner scanner = new Scanner(System.in);

        //Enter a URL to analyze any game on chess.com
        //If the user is not logged in the program will stop
        while (true) {
            System.out.println("\nEnter URL:");
            String url = scanner.nextLine();
            service.submit(new PageLoader(driver, url));
        }
    }
}

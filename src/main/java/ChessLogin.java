import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ChessLogin {
    private final WebDriverWait wait;

    public ChessLogin(WebDriver driver) {
        //This is the amount of time the driver will wait until the element is found
        //If the element is not found within the correct time, it will throw an error and the program will stop
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(30));

        //Get the login tab, when login is succesful this will return to the main chess.com page
        driver.get("https://www.chess.com/login_and_go?returnUrl=https://www.chess.com/");

        //Get the username field and send correct username
        WebElement username = waitForElement(By.id("username"));
        username.sendKeys(System.getenv("USERNAME"));

        //Get the password field and send correct password
        WebElement password = waitForElement(By.id("password"));
        password.sendKeys(System.getenv("PASSWORD"));

        //Click the login button
        WebElement loginButton = waitForElement(By.id("login"));
        loginButton.click();

        //Wait for the return to the home page, if the user is logged in the page has an attribute 'user-logged-in'
        //If this attribute does not exist it will exit the program
        waitForElement(By.cssSelector("html.user-logged-in"));
    }

    //This method waits until the element is found
    //Or it will throw an exception and the program is terminated
    private WebElement waitForElement(By element) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
}

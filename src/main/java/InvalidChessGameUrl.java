public class InvalidChessGameUrl extends Exception{
    public InvalidChessGameUrl() {
    }

    public InvalidChessGameUrl(String message) {
        super(message);
    }
}

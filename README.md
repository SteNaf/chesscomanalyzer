# ChessComAnalyzer

This is a chess.com analyzer I have built in Java using the Selenium library. Welcome to my project!
I have always found it annoying that you cannot see the accuracy of your chess.com games without having a
premium membership. That is why I have created this program.

All you need to do is add 2 environmental variables to the project:

USERNAME='valid username'; PASSWORD='valid password';

These are the variables that are used to log in to the specific account.

You will also need to make sure that this account has a premium membership.
A premium membership is offered for free at the creation of a new account.
All you need to do is create a new account, add a free premium membership (Don't forget to cancel), add the
account credentials to the environmental variables in this project and your good to go.

After running the program you will see that your computer is starting a browser window, and logging in to chess.com.
Then in the console of your preferred IDE you need to input a correct game URL. For example:

https://www.chess.com/analysis/game/live/50174420421

After inputting this url the browser tab will go to the page and analyze it.
After analyzing is complete, you can input another URL.